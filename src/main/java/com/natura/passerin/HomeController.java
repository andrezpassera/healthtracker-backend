package com.natura.passerin;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.natura.passerin.services.HistoryServices;
import com.natura.passerin.systemclass.History;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	private HistoryServices historyService; 
	/**
	 * Simply selects the home view to render by returning its name.
	 */
	@RequestMapping(value = "/history/list", method = RequestMethod.GET)
	@ResponseBody
	public List<History> home(Locale locale, Model model) {
		return historyService.getHistoryList();
	}
	
	/**
	 * Inserts new history.
	 */
	@RequestMapping(value = "/history/insert", method = RequestMethod.POST)
	@ResponseBody
	public int insertHistory(@RequestBody History history, Locale locale, Model model){
		return historyService.insertHistory(history);	
	}
	
	/**
	 * Updates history.
	 */
	@RequestMapping(value = "/history/edit", method = RequestMethod.POST)
	@ResponseBody
	public int updateHistory(@RequestBody History history, Locale locale, Model model){
		return historyService.updateHistory(history);	
	}
	
	/**
	 * Deletes selected history.
	 */
	@RequestMapping(value = "/history/delete/{idHistory}", method = RequestMethod.GET)
	@ResponseBody
	public int deleteHistory(@PathVariable("idHistory") String id, Locale locale, Model model) {
		return historyService.deleteHistory(id);
	}
	
	/**
	 * Deletes selected history.
	 */
	@RequestMapping(value = "/history/list/{name}", method = RequestMethod.GET)
	@ResponseBody
	public List<History> getHistoryListByName(@PathVariable("name") String name, Locale locale, Model model) {
//		History history2 = new History();
//		history2.setName("Hernan Gimenez");
//		history2.setBloodPreassure("13/8");
//		history2.setHeartRate("110");
//		history2.setDate("12/08/2018");
//		history2.setAge("58");
//		history2.setIdHistory("9");
		return historyService.getHistoryListByName(name);
	}
}
