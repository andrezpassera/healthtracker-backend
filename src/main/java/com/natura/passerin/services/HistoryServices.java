package com.natura.passerin.services;

import java.util.ArrayList;
import java.util.List;

import com.natura.passerin.systemclass.History;

public interface HistoryServices {
	List<History> getHistoryList();
	int insertHistory(History history);
	int deleteHistory(String idHistory);
	int updateHistory(History history);
	List<History> getHistoryListByName(String name);
}
