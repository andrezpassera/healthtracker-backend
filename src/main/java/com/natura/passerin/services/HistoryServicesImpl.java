package com.natura.passerin.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.natura.passerin.dao.HistoryDao;
import com.natura.passerin.systemclass.History;

@Service
public class HistoryServicesImpl implements HistoryServices{

	@Override
	public List<History> getHistoryList() {
		HistoryDao hst = new HistoryDao();
		return hst.getHistory();
	}

	@Override
	public int insertHistory(History history){
		HistoryDao hst = new HistoryDao();
		return hst.insertHistory(history);
	}

	@Override
	public int deleteHistory(String idHistory) {
		HistoryDao hst = new HistoryDao();
		return hst.deleteHistory(idHistory);
	}

	@Override
	public int updateHistory(History history) {
		HistoryDao hst = new HistoryDao();
		return hst.updateHistory(history);
	}

	@Override
	public List<History> getHistoryListByName(String name) {
		HistoryDao hst = new HistoryDao();
		return hst.getHistoryListByName(name);
	}
	

}
