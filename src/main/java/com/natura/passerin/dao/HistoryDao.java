package com.natura.passerin.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.natura.passerin.systemclass.History;

public class HistoryDao {
	
public List<History> getHistory(){
		List<History> stories = new ArrayList<History>();
		ConexionSQL mySQL = new ConexionSQL();
		       Connection cn = mySQL.conectar();
		       String sSQL="select id_history, name, age, blood_preassure, heart_rate, date from public.history";
		       try {
		           Statement st = cn.createStatement();
		           ResultSet rs = st.executeQuery(sSQL);
		           while(rs.next()){
		           	History story = new History();
		           	story.setIdHistory(rs.getString("id_history"));
		            story.setName(rs.getString(2));
		            story.setAge(rs.getString(3));
		            story.setBloodPreassure(rs.getString(4));
		            story.setHeartRate(rs.getString(5));
		            story.setDate(rs.getString(6));
		            stories.add(story);
		           }
		       } 
		       catch (SQLException ex) 
		       {
		           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
		       }
		       return stories;
	}


public int insertHistory(History history) {
	int i=0;
	ConexionSQL mySQL = new ConexionSQL();
	       Connection cn = mySQL.conectar();
	       String sSQL="insert into public.history (name,age,blood_preassure,heart_rate,date) values ('"+history.getName()+"','" + history.getAge() + "', '"+ history.getBloodPreassure() + "','" + history.getHeartRate() + "','" + history.getDate() + "')";
	       int result = 0;
	       try {
	    	   Statement st = cn.createStatement();
	    	   result = st.executeUpdate(sSQL);       
	       }catch (SQLException ex) {
	    	   result = -2;
	       }
	       return result;
}


public int deleteHistory(String id){
	 int result = 0;
	ConexionSQL mySQL = new ConexionSQL();
	       Connection cn = mySQL.conectar();
	       String sSQL="delete from public.history where id_history = '"+id+"'";

	       try {
	    	   Statement st = cn.createStatement();
	    	   result = st.executeUpdate(sSQL);       
	       }catch (SQLException ex) {
	    	   result = -2;
	       }
	       return result;
}

public int updateHistory(History history){
	 int result = 0;
	ConexionSQL mySQL = new ConexionSQL();
	       Connection cn = mySQL.conectar();
	       
	       String sSQL="update public.history set name = '" +
	       history.getName()+"', age = '"+ history.getAge() + "', blood_preassure = '" +
	    		   history.getBloodPreassure() + "', heart_rate = '" + 
	       history.getHeartRate() + "', date = '" + history.getDate() + "' where id_history = " + history.getIdHistory();

	       try {
	    	   Statement st = cn.createStatement();
	    	   result = st.executeUpdate(sSQL);       
	       }catch (SQLException ex) {
	    	   result = -2;
	       }
	    
	       return result;
}

public List<History> getHistoryListByName(String name){
	List<History> stories = new ArrayList<History>();
	ConexionSQL mySQL = new ConexionSQL();
	       Connection cn = mySQL.conectar();
	       String sSQL="select id_history, name, age, blood_preassure, heart_rate, date from public.history where name = '" + name + "'";
	       try {
	           Statement st = cn.createStatement();
	           ResultSet rs = st.executeQuery(sSQL);
	           while(rs.next()){
	           	History story = new History();
	        	story.setIdHistory(rs.getString("id_history"));
	            story.setName(rs.getString(2));
	            story.setAge(rs.getString(3));
	            story.setBloodPreassure(rs.getString(4));
	            story.setHeartRate(rs.getString(5));
	            story.setDate(rs.getString(6));
	            stories.add(story);
	           }
	       } 
	       catch (SQLException ex) 
	       {
	           //JOptionPane.showMessageDialog(null, "Error al realizar Informe de venta  "+ex);
	       }
	       return stories;
}


}
