package com.natura.passerin.dao;
import java.sql.*;
import org.slf4j.LoggerFactory;

import com.natura.passerin.utils.PropertiesUtils;

public class ConexionSQL {

	 public String bd= "";
	 public String url="";
	 public String user=""; 
	 public String pass="";
	 
	 public ConexionSQL(){
		 this.bd=PropertiesUtils.getProperty(PropertiesUtils.BD_NAME);
		 this.url=PropertiesUtils.getProperty(PropertiesUtils.BD_URL)+bd;
		 this.user=PropertiesUtils.getProperty(PropertiesUtils.BD_USER);
		 this.pass=PropertiesUtils.getProperty(PropertiesUtils.BD_PASS);
	 }
	 
	 public Connection conectar()
	 {
	   Connection link = null;
	   try
	   {
	    Class.forName(PropertiesUtils.getProperty(PropertiesUtils.BD_DRIVER));

	     link= DriverManager.getConnection(this.url,this.user,this.pass);
	    }
	     catch(Exception e)
	     {
	       //JOptionPane.showMessageDialog(null,e);
	     }
	   return link;
	 }
	
	
}


