package com.natura.passerin.systemclass;

public class History {
	private String idHistory;
	private String name;
	private String age;
	private String bloodPreassure;
	private String heartRate;
	private String date;
	public String getIdHistory() {
		return idHistory;
	}
	public void setIdHistory(String idHistory) {
		this.idHistory = idHistory;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAge() {
		return age;
	}
	public void setAge(String age) {
		this.age = age;
	}
	public String getBloodPreassure() {
		return bloodPreassure;
	}
	public void setBloodPreassure(String bloodPreassure) {
		this.bloodPreassure = bloodPreassure;
	}
	public String getHeartRate() {
		return heartRate;
	}
	public void setHeartRate(String heartRate) {
		this.heartRate = heartRate;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	
	
	
}
